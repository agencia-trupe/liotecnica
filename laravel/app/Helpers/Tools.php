<?php

namespace App\Helpers;

class Tools
{

    public static function isActive($route = '')
    {
        return str_is($route, \Route::currentRouteName()) ? 'active' : '';
    }

}
