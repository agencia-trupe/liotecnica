<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('areasDeAtuacao', \App\Models\AreaDeAtuacao::ordenados()->get([
                'slug', 'titulo_pt', 'titulo_en', 'titulo_es'
            ]));
            $view->with('tecnologias', \App\Models\Tecnologia::ordenados()->get([
                'slug', 'titulo_pt', 'titulo_en', 'titulo_es'
            ]));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
