<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('trabalhe-conosco', 'App\Models\TrabalheConosco');
		$router->model('tecnologias', 'App\Models\Tecnologia');
        $router->model('areas-de-atuacao', 'App\Models\AreaDeAtuacao');
		$router->model('subcategorias', 'App\Models\AreaDeAtuacaoSubcategoria');
		$router->model('responsabilidade-social', 'App\Models\ResponsabilidadeSocial');
		$router->model('quem-somos', 'App\Models\QuemSomos');
		$router->model('missao-e-visao', 'App\Models\MissaoEVisao');
		$router->model('garantia-de-qualidade', 'App\Models\GarantiaDeQualidade');
		$router->model('banners', 'App\Models\Banner');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('areaDeAtuacao', function($value) {
            return \App\Models\AreaDeAtuacao::whereSlug($value)->first() ?: abort('404');
        });
        $router->bind('tecnologia', function($value) {
            return \App\Models\Tecnologia::whereSlug($value)->first() ?: abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
