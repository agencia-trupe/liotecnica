<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TecnologiasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
            'imagem' => 'required|image',
            'infografico_pt' => 'required|image',
            'infografico_en' => 'required|image',
            'infografico_es' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
            $rules['infografico_pt'] = 'image';
            $rules['infografico_en'] = 'image';
            $rules['infografico_es'] = 'image';
        }

        return $rules;
    }
}
