<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MissaoEVisaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'subtitulo_1_pt' => 'required',
            'texto_1_pt' => 'required',
            'subtitulo_2_pt' => 'required',
            'texto_2_pt' => 'required',
            'subtitulo_3_pt' => 'required',
            'texto_3_pt' => 'required',
            'subtitulo_1_en' => 'required',
            'texto_1_en' => 'required',
            'subtitulo_2_en' => 'required',
            'texto_2_en' => 'required',
            'subtitulo_3_en' => 'required',
            'texto_3_en' => 'required',
            'subtitulo_1_es' => 'required',
            'texto_1_es' => 'required',
            'subtitulo_2_es' => 'required',
            'texto_2_es' => 'required',
            'subtitulo_3_es' => 'required',
            'texto_3_es' => 'required',
        ];
    }
}
