<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GarantiaDeQualidadeRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem' => 'image',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];
    }
}
