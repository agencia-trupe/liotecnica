<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class TrabalheConoscoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
            'imagem'   => 'image',
            'link'     => 'required',
            'botao_pt' => 'required',
            'botao_en' => 'required',
            'botao_es' => 'required',
        ];
    }
}
