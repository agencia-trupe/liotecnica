<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AreasDeAtuacaoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
            'capa' => 'required|image',
            'imagem' => 'required|image',
        ];

        if ($this->method() != 'POST') {
            $rules['capa'] = 'image';
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
