<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BannersRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'cor' => 'required',
            'imagem' => 'required|image',
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'titulo_es' => 'required',
            'subtitulo_pt' => 'required',
            'subtitulo_en' => 'required',
            'subtitulo_es' => 'required',
            'texto_pt' => 'required',
            'texto_en' => 'required',
            'texto_es' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
