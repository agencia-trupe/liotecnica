<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\MissaoEVisaoRequest;
use App\Http\Controllers\Controller;

use App\Models\MissaoEVisao;

class MissaoEVisaoController extends Controller
{
    public function index()
    {
        $registro = MissaoEVisao::first();

        return view('painel.missao-e-visao.edit', compact('registro'));
    }

    public function update(MissaoEVisaoRequest $request, MissaoEVisao $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = MissaoEVisao::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.missao-e-visao.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
