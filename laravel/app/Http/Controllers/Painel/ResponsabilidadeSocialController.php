<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ResponsabilidadeSocialRequest;
use App\Http\Controllers\Controller;

use App\Models\ResponsabilidadeSocial;

class ResponsabilidadeSocialController extends Controller
{
    public function index()
    {
        $registro = ResponsabilidadeSocial::first();

        return view('painel.responsabilidade-social.edit', compact('registro'));
    }

    public function update(ResponsabilidadeSocialRequest $request, ResponsabilidadeSocial $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = ResponsabilidadeSocial::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.responsabilidade-social.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
