<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TecnologiasRequest;
use App\Http\Controllers\Controller;

use App\Models\Tecnologia;

class TecnologiasController extends Controller
{
    public function index()
    {
        $registros = Tecnologia::ordenados()->get();

        return view('painel.tecnologias.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.tecnologias.create');
    }

    public function store(TecnologiasRequest $request)
    {
        try {

            $input = $request->all();
            $input['infografico_pt'] = Tecnologia::uploadInfograficoPt();
            $input['infografico_en'] = Tecnologia::uploadInfograficoEn();
            $input['infografico_es'] = Tecnologia::uploadInfograficoEs();
            $input['imagem'] = Tecnologia::uploadImagem();

            Tecnologia::create($input);
            return redirect()->route('painel.tecnologias.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Tecnologia $registro)
    {
        return view('painel.tecnologias.edit', compact('registro'));
    }

    public function update(TecnologiasRequest $request, Tecnologia $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['infografico_pt'])) $input['infografico_pt'] = Tecnologia::uploadInfograficoPt();
            if (isset($input['infografico_en'])) $input['infografico_en'] = Tecnologia::uploadInfograficoEn();
            if (isset($input['infografico_es'])) $input['infografico_es'] = Tecnologia::uploadInfograficoEs();
            if (isset($input['imagem'])) $input['imagem'] = Tecnologia::uploadImagem();

            $registro->update($input);
            return redirect()->route('painel.tecnologias.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Tecnologia $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.tecnologias.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
