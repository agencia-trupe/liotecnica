<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AreasDeAtuacaoSubcategoriasRequest;
use App\Http\Controllers\Controller;

use App\Models\AreaDeAtuacao;
use App\Models\AreaDeAtuacaoSubcategoria;

class AreasDeAtuacaoSubcategoriasController extends Controller
{
    public function index(AreaDeAtuacao $areaDeAtuacao)
    {
        $registros = AreaDeAtuacaoSubcategoria::area($areaDeAtuacao->id)
            ->ordenados()->get();

        return view('painel.areas-de-atuacao.subcategorias.index', compact('areaDeAtuacao', 'registros'));
    }

    public function create(AreaDeAtuacao $areaDeAtuacao)
    {
        return view('painel.areas-de-atuacao.subcategorias.create', compact('areaDeAtuacao'));
    }

    public function store(AreaDeAtuacao $areaDeAtuacao, AreasDeAtuacaoSubcategoriasRequest $request)
    {
        try {

            $input = $request->all();

            $areaDeAtuacao->subcategorias()->create($input);

            return redirect()->route('painel.areas-de-atuacao.subcategorias.index', $areaDeAtuacao->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(AreaDeAtuacao $areaDeAtuacao, AreaDeAtuacaoSubcategoria $registro)
    {
        return view('painel.areas-de-atuacao.subcategorias.edit', compact('areaDeAtuacao', 'registro'));
    }

    public function update(AreasDeAtuacaoSubcategoriasRequest $request, AreaDeAtuacao $areaDeAtuacao, AreaDeAtuacaoSubcategoria $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.areas-de-atuacao.subcategorias.index', $areaDeAtuacao->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(AreaDeAtuacao $areaDeAtuacao, AreaDeAtuacaoSubcategoria $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.areas-de-atuacao.subcategorias.index', $areaDeAtuacao->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
