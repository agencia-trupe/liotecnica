<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\GarantiaDeQualidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\GarantiaDeQualidade;

class GarantiaDeQualidadeController extends Controller
{
    public function index()
    {
        $registro = GarantiaDeQualidade::first();

        return view('painel.garantia-de-qualidade.edit', compact('registro'));
    }

    public function update(GarantiaDeQualidadeRequest $request, GarantiaDeQualidade $registro)
    {
        try {
            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) $input['imagem'] = GarantiaDeQualidade::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.garantia-de-qualidade.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
