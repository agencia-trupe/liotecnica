<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\TrabalheConoscoRequest;
use App\Http\Controllers\Controller;

use App\Models\TrabalheConosco;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $registro = TrabalheConosco::first();

        return view('painel.trabalhe-conosco.edit', compact('registro'));
    }

    public function update(TrabalheConoscoRequest $request, TrabalheConosco $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = TrabalheConosco::uploadImagem();

            $registro->update($input);

            return redirect()->route('painel.trabalhe-conosco.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
