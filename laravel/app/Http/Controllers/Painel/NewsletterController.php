<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Newsletter;

class NewsletterController extends Controller
{
    public function index()
    {
        $emails = Newsletter::orderBy('email', 'ASC')->paginate(20);

        return view('painel.newsletter.index', compact('emails'));
    }

    public function destroy(Newsletter $email)
    {
        try {

            $email->delete();
            return redirect()->route('painel.newsletter.index')->with('success', 'E-mail excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir e-mail: '.$e->getMessage()]);

        }
    }

    public function exportar()
    {
        $file = 'liotecnica-newsletter_'.date('d-m-Y_His');

        \Excel::create($file, function($excel) {
            $excel->sheet('emails', function($sheet) {
                $sheet->fromModel(Newsletter::orderBy('email', 'ASC')->get(['nome', 'email']));
            });
        })->download('csv');
    }
}
