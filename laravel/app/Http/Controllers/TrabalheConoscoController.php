<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\TrabalheConosco;

class TrabalheConoscoController extends Controller
{
    public function index()
    {
        $trabalheConosco = TrabalheConosco::first();

        return view('frontend.trabalhe-conosco', compact('trabalheConosco'));
    }
}
