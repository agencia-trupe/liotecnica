<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Tecnologia;

class TecnologiasController extends Controller
{
    public function index(Tecnologia $tecnologia)
    {
        $tecnologias = Tecnologia::ordenados()->get();

        if (! $tecnologia->exists) {
            $tecnologia = Tecnologia::ordenados()->first();

            if (! $tecnologia) abort('404');
        }

        return view('frontend.tecnologias', compact('tecnologias', 'tecnologia'));
    }
}
