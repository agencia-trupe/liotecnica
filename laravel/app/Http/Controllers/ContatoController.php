<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Newsletter;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function post(ContatosRecebidosRequest $request)
    {
        ContatoRecebido::create($request->except('newsletter'));

        $emailDeEnvio = $this->selecionaEmail($request->get('unidade'));

        \Mail::send('emails.contato', $request->all(), function($message) use ($request, $emailDeEnvio)
        {
            $message->to($emailDeEnvio, config('site.name'))
                    ->subject('[CONTATO] '.config('site.name'))
                    ->replyTo($request->get('email'), $request->get('nome'));
        });

        if ($this->enviaNewsletter($request)) {
            Newsletter::create([
                'nome'  => $request->get('nome'),
                'email' => $request->get('email'),
            ]);
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    private function selecionaEmail($unidade)
    {
        $qualimax = [
            'Varejo', 'Retail',
            'Food Service',
            'Institucional Público', 'Institutional Market', 'Alimentación Institucional'
        ];

        if (in_array($unidade, $qualimax)) return 'qualimax@liotecnica.com.br';
        return 'faleconosco@liotecnica.com.br';
    }

    private function enviaNewsletter(Request $request) {
        return $request->has('newsletter') && Newsletter::where('email', $request->get('email'))->count() == 0;
    }
}
