<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\QuemSomos;
use App\Models\MissaoEVisao;
use App\Models\GarantiaDeQualidade;

class EmpresaController extends Controller
{
    public function index($slug = 'quem-somos')
    {
        if ($slug === 'quem-somos') {
            $empresa = QuemSomos::first();
        } elseif ($slug === 'missao-e-visao') {
            $empresa = MissaoEVisao::first();
        } elseif ($slug === 'garantia-de-qualidade') {
            $empresa = GarantiaDeQualidade::first();
        } else {
            return abort('404');
        }

        return view('frontend.empresa.' . $slug, compact('empresa', 'slug'));
    }
}
