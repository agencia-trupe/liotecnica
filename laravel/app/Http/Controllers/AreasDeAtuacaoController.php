<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\AreaDeAtuacao;

class AreasDeAtuacaoController extends Controller
{
    public function index(AreaDeAtuacao $areaDeAtuacao)
    {
        if (! $areaDeAtuacao->exists) {
            $areasDeAtuacao = AreaDeAtuacao::ordenados()->get();

            return view('frontend.areas-de-atuacao.index', compact('areasDeAtuacao'));
        }

        $outrasAreas = AreaDeAtuacao::where('id', '!=', $areaDeAtuacao->id)
            ->ordenados()->take(5)->get();

        return view('frontend.areas-de-atuacao.show', compact('areaDeAtuacao', 'outrasAreas'));
    }
}
