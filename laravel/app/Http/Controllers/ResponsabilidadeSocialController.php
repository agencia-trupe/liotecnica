<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\ResponsabilidadeSocial;

class ResponsabilidadeSocialController extends Controller
{
    public function index()
    {
        $responsabilidadeSocial = ResponsabilidadeSocial::first();

        return view('frontend.responsabilidade-social', compact('responsabilidadeSocial'));
    }
}
