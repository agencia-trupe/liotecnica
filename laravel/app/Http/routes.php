<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('a-empresa/{slug?}', 'EmpresaController@index')->name('empresa');
    Route::get('areas-de-atuacao/{areaDeAtuacao?}', 'AreasDeAtuacaoController@index')->name('areas-de-atuacao');
    Route::get('responsabilidade-social', 'ResponsabilidadeSocialController@index')->name('responsabilidade-social');
    Route::get('tecnologias/{tecnologia?}', 'TecnologiasController@index')->name('tecnologias');
    Route::get('trabalhe-conosco', 'TrabalheConoscoController@index')->name('trabalhe-conosco');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato', 'ContatoController@post')->name('contato.post');

    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en' || $idioma == 'es') {
            Session::put('locale', $idioma);
        }
        return redirect()->back();
    })->name('lang');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('trabalhe-conosco', 'TrabalheConoscoController', ['only' => ['index', 'update']]);
		Route::resource('tecnologias', 'TecnologiasController');
        Route::resource('areas-de-atuacao', 'AreasDeAtuacaoController');
		Route::resource('areas-de-atuacao.subcategorias', 'AreasDeAtuacaoSubcategoriasController');
		Route::resource('responsabilidade-social', 'ResponsabilidadeSocialController', ['only' => ['index', 'update']]);
		Route::resource('quem-somos', 'QuemSomosController', ['only' => ['index', 'update']]);
		Route::resource('missao-e-visao', 'MissaoEVisaoController', ['only' => ['index', 'update']]);
		Route::resource('garantia-de-qualidade', 'GarantiaDeQualidadeController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
