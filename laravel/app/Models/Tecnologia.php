<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;

class Tecnologia extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'tecnologias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadInfograficoPt() {
        return CropImage::make('infografico_pt', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/tecnologias/infografico/'
        ]);
    }

    public static function uploadInfograficoEn() {
        return CropImage::make('infografico_en', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/tecnologias/infografico/'
        ]);
    }

    public static function uploadInfograficoEs() {
        return CropImage::make('infografico_es', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/tecnologias/infografico/'
        ]);
    }

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 310,
            'height' => null,
            'path'   => 'assets/img/tecnologias/imagem-lateral/'
        ]);
    }
}
