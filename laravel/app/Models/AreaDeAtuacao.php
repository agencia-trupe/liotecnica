<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

use App\Helpers\CropImage;

class AreaDeAtuacao extends Model implements SluggableInterface
{
    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'titulo_pt',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    protected $table = 'areas_de_atuacao';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public static function uploadCapa() {
        return CropImage::make('capa', [
            'width'  => 300,
            'height' => 125,
            'path'   => 'assets/img/areas-de-atuacao/capa/'
        ]);
    }

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 220,
            'height' => null,
            'path'   => 'assets/img/areas-de-atuacao/imagem-lateral/'
        ]);
    }

    public function subcategorias()
    {
        return $this->hasMany('App\Models\AreaDeAtuacaoSubcategoria', 'areas_de_atuacao_id')->ordenados();
    }
}
