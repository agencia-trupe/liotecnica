<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class ResponsabilidadeSocial extends Model
{
    protected $table = 'responsabilidade_social';

    protected $guarded = ['id'];

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 310,
            'height' => null,
            'path'   => 'assets/img/responsabilidade-social/'
        ]);
    }
}
