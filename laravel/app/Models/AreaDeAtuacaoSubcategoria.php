<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaDeAtuacaoSubcategoria extends Model
{
    protected $table = 'areas_de_atuacao_subcategorias';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

    public function scopeArea($query, $areas_de_atuacao_id)
    {
        return $query->where('areas_de_atuacao_id', $areas_de_atuacao_id);
    }

    public function areaDeAtuacao()
    {
        return $this->belongsTo('App\Models\AreaDeAtuacao', 'areas_de_atuacao_id');
    }
}
