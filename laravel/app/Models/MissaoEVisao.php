<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class MissaoEVisao extends Model
{
    protected $table = 'missao_e_visao';

    protected $guarded = ['id'];

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 310,
            'height' => null,
            'path'   => 'assets/img/a-empresa/'
        ]);
    }
}
