<?php

namespace App\Models;

use App\Helpers\CropImage;

use Illuminate\Database\Eloquent\Model;

class TrabalheConosco extends Model
{
    protected $table = 'trabalhe_conosco';

    protected $guarded = ['id'];

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 550,
            'height' => null,
            'path'   => 'assets/img/trabalhe-conosco/'
        ]);
    }
}
