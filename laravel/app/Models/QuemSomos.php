<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class QuemSomos extends Model
{
    protected $table = 'quem_somos';

    protected $guarded = ['id'];

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 310,
            'height' => null,
            'path'   => 'assets/img/a-empresa/'
        ]);
    }
}
