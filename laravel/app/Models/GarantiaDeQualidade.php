<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class GarantiaDeQualidade extends Model
{
    protected $table = 'garantia_de_qualidade';

    protected $guarded = ['id'];

    public static function uploadImagem() {
        return CropImage::make('imagem', [
            'width'  => 310,
            'height' => null,
            'path'   => 'assets/img/a-empresa/'
        ]);
    }
}
