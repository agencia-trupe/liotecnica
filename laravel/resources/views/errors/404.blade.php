@extends('frontend.common.template')

@section('content')

    <div class="not-found">
        <p class="not-found__message">{{ trans('frontend.404') }}</p>
    </div>

@endsection
