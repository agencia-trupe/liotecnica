@extends('frontend.common.template')

@section('content')

    <div class="content institucional">
        <div class="center">
            <div class="institucional__aside">
                @foreach($tecnologias as $tec)
                <a href="{{ route('tecnologias', $tec->slug) }}" @if($tec->slug === $tecnologia->slug) class="active" @endif>
                    &raquo; {{ $tec->{'titulo_'.app()->getLocale()} }}
                </a>
                @endforeach
            </div>

            <div class="institucional__text">
                <h1>{{ $tecnologia->{'titulo_'.app()->getLocale()} }}</h1>
                {!! $tecnologia->{'texto_'.app()->getLocale()} !!}

                <img class="tecnologias-infografico" src="{{ asset('assets/img/tecnologias/infografico/'.$tecnologia->{'infografico_'.app()->getLocale()}) }}" alt="">
            </div>

            <div class="institucional__aside">
                <img src="{{ asset('assets/img/tecnologias/imagem-lateral/'.$tecnologia->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
