@extends('frontend.common.template')

@section('content')

    <div class="content areas-de-atuacao__index">
        <div class="center">
            @foreach($areasDeAtuacao as $area)
            <a href="{{ route('areas-de-atuacao', $area->slug) }}">
                <img src="{{ asset('assets/img/areas-de-atuacao/capa/'.$area->capa) }}" alt="">
                <span>{{ $area->{'titulo_'.app()->getLocale()} }}</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
