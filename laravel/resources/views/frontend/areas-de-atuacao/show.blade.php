@extends('frontend.common.template')

@section('content')

    <div class="content areas-de-atuacao__show">
        <div class="center">
            <div class="areas-de-atuacao__aside">
                <img src="{{ asset('assets/img/areas-de-atuacao/imagem-lateral/'.$areaDeAtuacao->imagem) }}" alt="">
            </div>

            <div class="areas-de-atuacao__text">
                <div class="areas-de-atuacao__text-wrapper">
                    <h1>{{ $areaDeAtuacao->{'titulo_'.app()->getLocale()} }}</h1>
                    {!! $areaDeAtuacao->{'texto_'.app()->getLocale()} !!}
                </div>

                <div class="areas-de-atuacao__subcategorias">
                    @foreach($areaDeAtuacao->subcategorias as $subcategoria)
                    <div class="subcategoria subcategoria--{{ $subcategoria->id }}">
                        <style>
                            .subcategoria--{{ $subcategoria->id }} .subcategoria__handle h2 {
                                color: #{{ $subcategoria->cor }};
                            }
                            .subcategoria--{{ $subcategoria->id }} .subcategoria__handle.active {
                                background: #{{ $subcategoria->cor }};
                            }

                            .subcategoria--{{ $subcategoria->id }} .areas-de-atuacao-subtitulo {
                                border-top: 1px solid #{{ $subcategoria->cor }};
                                color: #{{ $subcategoria->cor }};
                            }
                        </style>
                        <a href="#" class="subcategoria__handle">
                            <svg xmlns="http://www.w3.org/2000/svg" width="21.333" height="21.333" viewBox="0 0 564 564" shape-rendering="geometricPrecision" text-rendering="geometricPrecision" image-rendering="optimizeQuality" fill-rule="evenodd" clip-rule="evenodd"><path fill="#{{ $subcategoria->cor }}" d="M256 286L482 60l64 65-290 290-146-146 65-65 81 82zm216-24h92v20c0 155-127 282-282 282S0 437 0 282 127 0 282 0c36 0 72 8 106 19l31 11-73 72-11-3c-18-5-35-7-53-7-104 0-190 86-190 190 0 105 86 190 190 190 105 0 190-85 190-190v-20z" id="Camada_x0020_1"/></svg>
                            <h2>{{ $subcategoria->{'titulo_'.app()->getLocale()} }}</h2>
                        </a>
                        <div class="subcategoria__content">
                            {!! $subcategoria->{'texto_'.app()->getLocale()} !!}
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <div class="areas-de-atuacao__outras">
        <div class="center">
            <p>{{ trans('frontend.areas-de-atuacao.outras') }}</p>
            @foreach($outrasAreas as $area)
            <a href="{{ route('areas-de-atuacao', $area->slug) }}">
                <img src="{{ asset('assets/img/areas-de-atuacao/capa/'.$area->capa) }}" alt="">
                <span>{{ $area->{'titulo_'.app()->getLocale()} }}</span>
            </a>
            @endforeach
        </div>
    </div>

@endsection
