@extends('frontend.common.template')

@section('content')

    <div class="content institucional">
        <div class="center">
            <div class="institucional__text">
                <h1>{{ trans('frontend.responsabilidade-social.responsabilidade-social') }}</h1>
                {!! $responsabilidadeSocial->{'texto_'.app()->getLocale()} !!}
            </div>

            <div class="institucional__aside">
                <img src="{{ asset('assets/img/responsabilidade-social/'.$responsabilidadeSocial->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
