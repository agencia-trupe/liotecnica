@extends('frontend.common.template')

@section('content')

    <div class="content trabalhe-conosco">
        <div class="center">
            <div class="trabalhe-conosco__text">
                <h1>{{ trans('frontend.trabalhe-conosco') }}</h1>
                {!! $trabalheConosco->{'texto_'.app()->getLocale()} !!}
                <a href="{{ $trabalheConosco->link }}" class="botao" target="_blank">
                    {{ $trabalheConosco->{'botao_'.app()->getLocale()} }}
                </a>
            </div>

            <div class="trabalhe-conosco__aside">
                <img src="{{ asset('assets/img/trabalhe-conosco/'.$trabalheConosco->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
