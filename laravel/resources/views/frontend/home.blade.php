@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <div class="banner">
            <div class="center">
                <img src="{{ asset('assets/img/banners/'.$banner->imagem) }}" alt="" class="banner__imagem">
                <h1 class="banner__titulo" style="color: #{{ $banner->cor }}">
                    {{ $banner->{'titulo_'.app()->getLocale()} }}
                </h1>
            </div>
            <div class="banner__gradient-wrapper">
                <div class="banner__gradient" style="background-image:-webkit-linear-gradient(180deg,#fff 30%,#{{ $banner->cor }} 100%);background-image:-moz-linear-gradient(180deg,#fff 30%,#{{ $banner->cor }} 100%);background-image:-ms-linear-gradient(180deg,#fff 30%,#{{ $banner->cor }} 100%);background-image:-o-linear-gradient(180deg,#fff 30%,#{{ $banner->cor }} 100%);background-image:linear-gradient(0deg,#fff 30%,#{{ $banner->cor }} 100%);"></div>
                <div class="center">
                    <div class="banner__textos">
                        <h2 class="banner__subtitulo" style="color: #{{ $banner->cor }}">
                            {{ $banner->{'subtitulo_'.app()->getLocale()} }}
                        </h2>
                        <p class="banner__texto">
                            {{ $banner->{'texto_'.app()->getLocale()} }}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        <div class="cycle-pager"></div>
    </div>

@endsection
