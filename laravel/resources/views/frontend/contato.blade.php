@extends('frontend.common.template')

@section('content')

    <div class="content contato">
        <div class="center">
            <div class="contato__col">
                <div class="contato__info">
                    <h1>{{ trans('frontend.contato.contato') }}</h1>
                    {!! $contato->{'contato_'.app()->getLocale()} !!}
                </div>

                <form class="contato__form" id="form-contato" method="POST">
                    <h1>{{ trans('frontend.contato.fale-conosco') }}</h1>
                    <select name="unidade" id="unidade" required>
                        <option value="" selected>{{ trans('frontend.contato.unidade') }}</option>
                        @foreach(range(1,7) as $i)
                        <option value="{{ trans('frontend.contato.unidade-'.$i) }}">{{ trans('frontend.contato.unidade-'.$i) }}</option>
                        @endforeach
                    </select>
                    <input type="text" name="nome" id="nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                    <input type="text" name="empresa" id="empresa" placeholder="{{ trans('frontend.contato.empresa') }}">
                    <input type="text" name="telefone" id="telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                    <input type="email" name="email" id="email" placeholder="E-mail" required>
                    <textarea name="mensagem" id="mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                    <label>
                        <input type="checkbox" name="newsletter" id="newsletter" value="1" checked>
                        <span>{{ trans('frontend.contato.newsletter') }}</span>
                    </label>
                    <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                    <div id="form-contato-response"></div>
                </form>
            </div>

            <div class="contato__col">
                <div class="contato__info">
                    <h1>{{ trans('frontend.contato.localizacao') }}</h1>
                    {!! $contato->{'localizacao_'.app()->getLocale()} !!}
                </div>

                <div class="contato__mapa">
                    {!! $contato->google_maps !!}
                </div>
            </div>
        </div>
    </div>

@endsection
