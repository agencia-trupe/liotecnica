@extends('frontend.empresa._template')

@section('text')

    <h1>{{ trans('frontend.empresa.missao-e-visao') }}</h1>

    <div class="text__col">
        <h2>{!! $empresa->{'subtitulo_1_'.app()->getLocale()} !!}</h2>
        {!! $empresa->{'texto_1_'.app()->getLocale()} !!}
    </div>

    <div class="text__col text__col--clear">
        <h2>{!! $empresa->{'subtitulo_2_'.app()->getLocale()} !!}</h2>
        {!! $empresa->{'texto_2_'.app()->getLocale()} !!}
    </div>

    <div class="text__col">
        <h2>{!! $empresa->{'subtitulo_3_'.app()->getLocale()} !!}</h2>
        {!! $empresa->{'texto_3_'.app()->getLocale()} !!}
    </div>

@endsection
