@extends('frontend.common.template')

@section('content')

    <div class="content institucional">
        <div class="center">
            <div class="institucional__aside">
                <a href="{{ route('empresa', 'quem-somos') }}" @if($slug === 'quem-somos') class="active" @endif>
                    &raquo; {{ trans('frontend.empresa.quem-somos') }}
                </a>
                <a href="{{ route('empresa', 'missao-e-visao') }}" @if($slug === 'missao-e-visao') class="active" @endif>
                    &raquo; {{ trans('frontend.empresa.missao-e-visao') }}
                </a>
                <a href="{{ route('empresa', 'garantia-de-qualidade') }}" @if($slug === 'garantia-de-qualidade') class="active" @endif>
                    &raquo; {{ trans('frontend.empresa.garantia-de-qualidade') }}
                </a>
            </div>

            <div class="institucional__text">
                @yield('text')
            </div>

            <div class="institucional__aside">
                <img src="{{ asset('assets/img/a-empresa/'.$empresa->imagem) }}" alt="">
            </div>
        </div>
    </div>

@endsection
