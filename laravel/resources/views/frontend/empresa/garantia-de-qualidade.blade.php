@extends('frontend.empresa._template')

@section('text')

    <h1>{{ trans('frontend.empresa.garantia-de-qualidade') }}</h1>

    {!! $empresa->{'texto_'.app()->getLocale()} !!}

@endsection
