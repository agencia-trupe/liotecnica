@extends('frontend.empresa._template')

@section('text')

    <h1>{{ trans('frontend.empresa.quem-somos') }}</h1>

    {!! $empresa->{'texto_'.app()->getLocale()} !!}

    <h2>{!! $empresa->{'subtitulo_1_'.app()->getLocale()} !!}</h2>
    {!! $empresa->{'texto_1_'.app()->getLocale()} !!}

    <h2>{!! $empresa->{'subtitulo_2_'.app()->getLocale()} !!}</h2>
    {!! $empresa->{'texto_2_'.app()->getLocale()} !!}

@endsection
