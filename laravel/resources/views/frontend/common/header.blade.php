    <header class="header">
        <div class="center">
            <a href="{{ route('home') }}" class="header__logo">{{ config('site.name') }}</a>
            <nav class="header__nav">
                @include('frontend.common._nav')
            </nav>

            <button class="mobile__toggle" type="button" role="button">
                <span class="lines"></span>
            </button>

            <div class="header__lang">
                @include('frontend.common._lang')
            </div>
        </div>

        <nav class="mobile__nav">
            @include('frontend.common._nav')
        </nav>
    </header>
