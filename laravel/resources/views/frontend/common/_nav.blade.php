<a href="{{ route('home') }}" class="{{ Tools::isActive('home') }}">
    {{ trans('frontend.home') }}
</a>
<a href="{{ route('empresa') }}" class="{{ Tools::isActive('empresa') }}">
    {{ trans('frontend.empresa.a-empresa') }}
</a>
<a href="{{ route('areas-de-atuacao') }}" class="{{ Tools::isActive('areas-de-atuacao') }}">
    {{ trans('frontend.areas-de-atuacao.areas-de-atuacao') }}
</a>
<a href="{{ route('tecnologias') }}" class="{{ Tools::isActive('tecnologias') }}">
    {{ trans('frontend.tecnologias.tecnologias') }}
</a>
<a href="{{ route('responsabilidade-social') }}" class="{{ Tools::isActive('responsabilidade-social') }}">
    {{ trans('frontend.responsabilidade-social.responsabilidade-social') }}
</a>
<a href="{{ route('trabalhe-conosco') }}" class="{{ Tools::isActive('trabalhe-conosco') }}">
    {{ trans('frontend.trabalhe-conosco') }}
</a>
<a href="{{ route('contato') }}" class="{{ Tools::isActive('contato') }}">
    {{ trans('frontend.contato.contato') }}
</a>
@if($contato->facebook)
<a href="{{ $contato->facebook }}" target="_blank" class="social social--facebook">facebook</a>
@endif
@if($contato->linkedin)
<a href="{{ $contato->linkedin }}" target="_blank" class="social social--linkedin">linkedin</a>
@endif
