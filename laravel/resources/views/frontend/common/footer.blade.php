    <footer class="footer">
        <div class="center">
            <div class="footer__col-wrapper">
                <div class="footer__col footer__col--links">
                    <a href="{{ route('empresa') }}" class="footer__link footer__link--titulo">
                        {{ trans('frontend.empresa.a-empresa') }}
                    </a>
                    <a href="{{ route('empresa', 'quem-somos') }}" class="footer__link">
                        {{ trans('frontend.empresa.quem-somos') }}
                    </a>
                    <a href="{{ route('empresa', 'missao-e-visao') }}" class="footer__link">
                        {{ trans('frontend.empresa.missao-e-visao') }}
                    </a>
                    <a href="{{ route('empresa', 'garantia-de-qualidade') }}" class="footer__link">
                        {{ trans('frontend.empresa.garantia-de-qualidade') }}
                    </a>
                </div>

                <div class="footer__col footer__col--links">
                    <a href="{{ route('areas-de-atuacao') }}" class="footer__link footer__link--titulo">
                        {{ trans('frontend.areas-de-atuacao.areas-de-atuacao') }}
                    </a>
                    @foreach($areasDeAtuacao as $area)
                    <a href="{{ route('areas-de-atuacao', $area->slug) }}" class="footer__link">
                        {{ $area->{'titulo_'.app()->getLocale()} }}
                    </a>
                    @endforeach
                </div>

                <div class="footer__col footer__col--links">
                    <a href="{{ route('tecnologias') }}" class="footer__link footer__link--titulo">
                        {{ trans('frontend.tecnologias.tecnologias') }}
                    </a>
                    @foreach($tecnologias as $tecnologia)
                    <a href="{{ route('tecnologias', $tecnologia->slug) }}" class="footer__link">
                        {{ $tecnologia->{'titulo_'.app()->getLocale()} }}
                    </a>
                    @endforeach
                </div>

                <div class="footer__col footer__col--links">
                    <a href="{{ route('responsabilidade-social') }}" class="footer__link footer__link--titulo">
                        {{ trans('frontend.responsabilidade-social.responsabilidade-social') }}
                    </a>
                    <a href="{{ route('trabalhe-conosco') }}" class="footer__link footer__link--titulo">
                        {{ trans('frontend.trabalhe-conosco') }}
                    </a>
                    <a href="{{ route('contato') }}" class="footer__link footer__link--titulo">
                        {{ trans('frontend.contato.contato') }}
                    </a>
                    @if(app()->getLocale() === 'pt')
                    <a href="http://www.portaldeboletos.com.br/liotecnica?ctr=acessoSacado" class="footer__link footer__link--box first">
                        EMISSÃO DE BOLETOS
                    </a>
                    <a href="http://cadastro.qualimax.com.br/fusion/portal" class="footer__link footer__link--box">
                        CADASTRO DE CLIENTES
                    </a>
                    @endif
                </div>

                <div class="footer__col footer__col--informacoes">
                    <img src="{{ asset('assets/img/layout/liotecnica-footer.png') }}" alt="Liotécnica">
                    <p>
                        {{ $contato->telefone }}<br>
                        <a href="mailto:{{ $contato->email }}">{{ $contato->email }}</a>
                    </p>
                </div>

                <div class="footer__col footer__col--copyright">
                    <p style="margin-bottom:16px">
                        <a href="http://www.finep.gov.br/" target="_blank">
                            <img src="{{ asset('assets/img/layout/finep.png') }}" alt="">
                        </a>
                        <span style="display:block">Inovando em parceria com a FINEP</span>
                    </p>
                    <p>© {{ date('Y') }} {{ config('site.name') }} - {{ trans('frontend.footer.copyright') }}</p>
                    <p>
                        <a href="http://www.trupe.net" target="_blank">{{ trans('frontend.footer.criacao-de-sites') }}</a>:
                        <a href="http://www.trupe.net" target="_blank">Trupe Agência Criativa</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
