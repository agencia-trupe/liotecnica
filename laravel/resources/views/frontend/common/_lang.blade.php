@foreach([
    'pt' => 'versão em português',
    'en' => 'english version',
    'es' => 'version en español'
] as $lang => $texto)
    @unless(app()->getLocale() === $lang)
        <a href="{{ route('lang', $lang) }}" class="lang__icon lang__icon--{{ $lang }}">{{ $texto }}</a>
    @endunless
@endforeach
