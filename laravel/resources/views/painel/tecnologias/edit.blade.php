@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tecnologias /</small> Editar Tecnologia</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.tecnologias.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.tecnologias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
