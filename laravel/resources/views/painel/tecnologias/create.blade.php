@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Tecnologias /</small> Adicionar Tecnologia</h2>
    </legend>

    {!! Form::open(['route' => 'painel.tecnologias.store', 'files' => true]) !!}

        @include('painel.tecnologias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
