@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <div class="well form-group">
            {!! Form::label('infografico_pt', 'Infográfico PT (largura máxima: 650px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/tecnologias/infografico/'.$registro->infografico_pt) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('infografico_pt', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <div class="well form-group">
            {!! Form::label('infografico_en', 'Infográfico EN (largura máxima: 650px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/tecnologias/infografico/'.$registro->infografico_en) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('infografico_en', ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('titulo_es', 'Título ES') !!}
            {!! Form::text('titulo_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_es', 'Texto ES') !!}
            {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <div class="well form-group">
            {!! Form::label('infografico_es', 'Infográfico ES (largura máxima: 650px)') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/tecnologias/infografico/'.$registro->infografico_es) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
        @endif
            {!! Form::file('infografico_es', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem lateral') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/tecnologias/imagem-lateral/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.tecnologias.index') }}" class="btn btn-default btn-voltar">Voltar</a>
