<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li class="dropdown @if(str_is('painel.quem-somos*', Route::currentRouteName()) || str_is('painel.missao-e-visao*', Route::currentRouteName()) || str_is('painel.garantia-de-qualidade*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            A Empresa
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.quem-somos.index') }}">Quem Somos</a></li>
            <li><a href="{{ route('painel.missao-e-visao.index') }}">Missão e Visão</a></li>
            <li><a href="{{ route('painel.garantia-de-qualidade.index') }}">Garantia de Qualidade</a></li>
        </ul>
    </li>
    <li @if(str_is('painel.areas-de-atuacao*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.areas-de-atuacao.index') }}">Áreas de Atuação</a>
    </li>
    <li @if(str_is('painel.responsabilidade-social*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.responsabilidade-social.index') }}">Responsabilidade Social</a>
    </li>
	<li @if(str_is('painel.tecnologias*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.tecnologias.index') }}">Tecnologias</a>
	</li>
    <li @if(str_is('painel.trabalhe-conosco*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.trabalhe-conosco.index') }}">Trabalhe Conosco</a>
    </li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
