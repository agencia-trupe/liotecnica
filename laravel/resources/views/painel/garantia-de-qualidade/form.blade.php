@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/a-empresa/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_es', 'Texto ES') !!}
            {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
