@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Empresa /</small> Garantia de Qualidade</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.garantia-de-qualidade.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.garantia-de-qualidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
