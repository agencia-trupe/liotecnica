@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/a-empresa/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_1_pt', 'Subtítulo 1 PT') !!}
            {!! Form::text('subtitulo_1_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_pt', 'Texto 1 PT') !!}
            {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_2_pt', 'Subtítulo 2 PT') !!}
            {!! Form::text('subtitulo_2_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_1_en', 'Subtítulo 1 EN') !!}
            {!! Form::text('subtitulo_1_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_en', 'Texto 1 EN') !!}
            {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_2_en', 'Subtítulo 2 EN') !!}
            {!! Form::text('subtitulo_2_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_es', 'Texto ES') !!}
            {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_1_es', 'Subtítulo 1 ES') !!}
            {!! Form::text('subtitulo_1_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_es', 'Texto 1 ES') !!}
            {!! Form::textarea('texto_1_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_2_es', 'Subtítulo 2 ES') !!}
            {!! Form::text('subtitulo_2_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_es', 'Texto 2 ES') !!}
            {!! Form::textarea('texto_2_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>
</div>



{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
