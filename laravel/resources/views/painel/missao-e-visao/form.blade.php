@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/a-empresa/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('subtitulo_1_pt', 'Subtítulo 1 PT') !!}
            {!! Form::text('subtitulo_1_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_pt', 'Texto 1 PT') !!}
            {!! Form::textarea('texto_1_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_2_pt', 'Subtítulo 2 PT') !!}
            {!! Form::text('subtitulo_2_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_pt', 'Texto 2 PT') !!}
            {!! Form::textarea('texto_2_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_3_pt', 'Subtítulo 3 PT') !!}
            {!! Form::text('subtitulo_3_pt', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_pt', 'Texto 3 PT') !!}
            {!! Form::textarea('texto_3_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('subtitulo_1_en', 'Subtítulo 1 EN') !!}
            {!! Form::text('subtitulo_1_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_en', 'Texto 1 EN') !!}
            {!! Form::textarea('texto_1_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_2_en', 'Subtítulo 2 EN') !!}
            {!! Form::text('subtitulo_2_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_en', 'Texto 2 EN') !!}
            {!! Form::textarea('texto_2_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_3_en', 'Subtítulo 3 EN') !!}
            {!! Form::text('subtitulo_3_en', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_en', 'Texto 3 EN') !!}
            {!! Form::textarea('texto_3_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('subtitulo_1_es', 'Subtítulo 1 ES') !!}
            {!! Form::text('subtitulo_1_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_1_es', 'Texto 1 ES') !!}
            {!! Form::textarea('texto_1_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_2_es', 'Subtítulo 2 ES') !!}
            {!! Form::text('subtitulo_2_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_2_es', 'Texto 2 ES') !!}
            {!! Form::textarea('texto_2_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
        <hr>
        <div class="form-group">
            {!! Form::label('subtitulo_3_es', 'Subtítulo 3 ES') !!}
            {!! Form::text('subtitulo_3_es', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('texto_3_es', 'Texto 3 ES') !!}
            {!! Form::textarea('texto_3_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>

</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
