@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>A Empresa /</small> Missão e Visão</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.missao-e-visao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.missao-e-visao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
