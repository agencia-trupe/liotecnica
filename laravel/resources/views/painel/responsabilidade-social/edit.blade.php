@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Responsabilidade Social</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.responsabilidade-social.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.responsabilidade-social.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
