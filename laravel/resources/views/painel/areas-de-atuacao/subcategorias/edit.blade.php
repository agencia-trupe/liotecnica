@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Atuação / {{ $areaDeAtuacao->titulo_pt }} /</small> Editar Subcategoria</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.areas-de-atuacao.subcategorias.update', $areaDeAtuacao->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.areas-de-atuacao.subcategorias.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
