@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <a href="{{ route('painel.areas-de-atuacao.index') }}" class="btn btn-sm btn-default">
        &larr; Voltar para Áreas de Atuação
    </a>

    <legend>
        <h2>
            <small>Áreas de Atuação / {{ $areaDeAtuacao->titulo_pt }} /</small> Subcategorias
            <a href="{{ route('painel.areas-de-atuacao.subcategorias.create', $areaDeAtuacao->id) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Subcategoria</a>
        </h2>
    </legend>

    @if(!count($registros))
    <div class="alert alert-warning" role="alert">Nenhum registro encontrado.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-info table-sortable" data-table="areas_de_atuacao_subcategorias">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título PT</th>
                <th class="no-filter"><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($registros as $registro)
            <tr class="tr-row" id="{{ $registro->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td><span style="background:#{{ $registro->cor }};padding:5px;border-radius:3px">{{ $registro->titulo_pt }}</span></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.areas-de-atuacao.subcategorias.destroy', $areaDeAtuacao->id, $registro->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.areas-de-atuacao.subcategorias.edit', [$areaDeAtuacao->id, $registro->id] ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>


    @endif

@endsection
