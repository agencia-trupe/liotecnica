@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Atuação / {{ $areaDeAtuacao->titulo_pt }} /</small> Adicionar Subcategoria</h2>
    </legend>

    {!! Form::open(['route' => ['painel.areas-de-atuacao.subcategorias.store', $areaDeAtuacao->id], 'files' => true]) !!}

        @include('painel.areas-de-atuacao.subcategorias.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
