@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_pt', 'Texto PT') !!}
            {!! Form::textarea('texto_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_en', 'Texto EN') !!}
            {!! Form::textarea('texto_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('texto_es', 'Texto ES') !!}
            {!! Form::textarea('texto_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'basic']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('link', 'Link Botão') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('botao_pt', 'Texto Botão PT') !!}
            {!! Form::text('botao_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('botao_en', 'Texto Botão EN') !!}
            {!! Form::text('botao_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('botao_es', 'Texto Botão ES') !!}
            {!! Form::text('botao_es', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/trabalhe-conosco/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
