@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('email', 'E-mail') !!}
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('telefone', 'Telefone') !!}
            {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('facebook', 'Facebook (opcional)') !!}
            {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('linkedin', 'Linkedin (opcional)') !!}
            {!! Form::text('linkedin', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('contato_pt', 'Contato PT') !!}
            {!! Form::textarea('contato_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'contato']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('contato_en', 'Contato EN') !!}
            {!! Form::textarea('contato_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'contato']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('contato_es', 'Contato ES') !!}
            {!! Form::textarea('contato_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'contato']) !!}
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('localizacao_pt', 'Localização PT') !!}
            {!! Form::textarea('localizacao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'contato']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('localizacao_en', 'Localização EN') !!}
            {!! Form::textarea('localizacao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'contato']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            {!! Form::label('localizacao_es', 'Localização ES') !!}
            {!! Form::textarea('localizacao_es', null, ['class' => 'form-control ckeditor', 'data-editor' => 'contato']) !!}
        </div>
    </div>
</div>

<hr>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
