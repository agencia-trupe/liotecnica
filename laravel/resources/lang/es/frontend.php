<?php
    return [
        'home' => 'Home',

        'empresa' => [
            'a-empresa'             => 'Empresa',
            'quem-somos'            => 'Quiénes Somos',
            'missao-e-visao'        => 'Misión y Visión',
            'garantia-de-qualidade' => 'Garantía de la Calidad'
        ],

        'areas-de-atuacao' => [
            'areas-de-atuacao' => 'Unidades de Negocio',
            'outras'           => 'Más información sobre nuestras otras áreas de operación'
        ],

        'responsabilidade-social' => [
            'responsabilidade-social' => 'Responsabilidad Social'
        ],

        'tecnologias' => [
            'tecnologias' => 'Tecnologías'
        ],

        'trabalhe-conosco' => 'Trabaja con nosotros',

        'contato' => [
            'contato'      => 'Contacto',
            'fale-conosco' => 'Hable con nosotros',
            'localizacao'  => 'Localización',
            'unidade'      => 'Unidade de Negocio',
            'unidade-1'    => 'Ingredientes Industriales',
            'unidade-2'    => 'Marca Propia',
            'unidade-3'    => 'Exportación',
            'unidade-4'    => 'Retail',
            'unidade-5'    => 'Food Service',
            'unidade-6'    => 'Alimentación Institucional',
            'unidade-7'    => 'Otros',
            'empresa'      => 'Empresa',
            'nome'         => 'Nombre completo',
            'telefone'     => 'Teléfono',
            'mensagem'     => 'Mensaje',
            'newsletter'   => 'Autorizo a recibir las novedades de Liotécnica por e-mail',
            'enviar'       => 'Enviar',
            'erro'         => 'Por favor, complete todos los campos correctamente',
            'sucesso'      => 'Mensaje enviado correctamente!'
        ],

        'footer' => [
            'copyright'        => 'Reservados todos los derechos',
            'criacao-de-sites' => 'Website'
        ],

        '404' => 'Página no encontrada'
    ];
