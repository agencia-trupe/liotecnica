<?php
    return [
        'home' => 'Home',

        'empresa' => [
            'a-empresa'             => 'Company',
            'quem-somos'            => 'About us',
            'missao-e-visao'        => 'Mission and Vision',
            'garantia-de-qualidade' => 'Quality Assurance'
        ],

        'areas-de-atuacao' => [
            'areas-de-atuacao' => 'Business Units',
            'outras'           => 'Learn more about our other areas of operation'
        ],

        'responsabilidade-social' => [
            'responsabilidade-social' => 'Social Responsability'
        ],

        'tecnologias' => [
            'tecnologias' => 'Technologies'
        ],

        'trabalhe-conosco' => 'Careers',

        'contato' => [
            'contato'      => 'Contact',
            'fale-conosco' => 'Contact us',
            'localizacao'  => 'Locations',
            'unidade'      => 'Business Units',
            'unidade-1'    => 'Food Ingredients',
            'unidade-2'    => 'Private Label',
            'unidade-3'    => 'Exports',
            'unidade-4'    => 'Retail',
            'unidade-5'    => 'Food Service',
            'unidade-6'    => 'Institutional Market',
            'unidade-7'    => 'Others',
            'empresa'      => 'Company',
            'nome'         => 'Full name',
            'telefone'     => 'Phone Number',
            'mensagem'     => 'Message',
            'newsletter'   => 'I authorize Liotécnica to send me news via e-mail',
            'enviar'       => 'Send',
            'erro'         => 'Fill in all fields correctly',
            'sucesso'      => 'Your message has been sent successfully!'
        ],

        'footer' => [
            'copyright'        => 'All rights reserved',
            'criacao-de-sites' => 'Website'
        ],

        '404' => 'Error: page not found'
    ];
