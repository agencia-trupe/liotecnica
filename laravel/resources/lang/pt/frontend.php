<?php
    return [
        'home' => 'Home',

        'empresa' => [
            'a-empresa'             => 'A Empresa',
            'quem-somos'            => 'Quem Somos',
            'missao-e-visao'        => 'Missão e Visão',
            'garantia-de-qualidade' => 'Garantia de Qualidade'
        ],

        'areas-de-atuacao' => [
            'areas-de-atuacao' => 'Áreas de Atuação',
            'outras'           => 'Saiba mais sobre nossas outras áreas de atuação'
        ],

        'responsabilidade-social' => [
            'responsabilidade-social' => 'Responsabilidade Social'
        ],

        'tecnologias' => [
            'tecnologias' => 'Tecnologias'
        ],

        'trabalhe-conosco' => 'Trabalhe conosco',

        'contato' => [
            'contato'      => 'Contato',
            'fale-conosco' => 'Fale conosco',
            'localizacao'  => 'Localização',
            'unidade'      => 'Unidade de Negócio',
            'unidade-1'    => 'Ingredientes Industriais',
            'unidade-2'    => 'Marcas Próprias',
            'unidade-3'    => 'Exportação',
            'unidade-4'    => 'Varejo',
            'unidade-5'    => 'Food Service',
            'unidade-6'    => 'Institucional Público',
            'unidade-7'    => 'Outros',
            'empresa'      => 'Empresa',
            'nome'         => 'Nome',
            'telefone'     => 'Telefone',
            'mensagem'     => 'Mensagem',
            'newsletter'   => 'Autorizo receber as novidades da Liotécnica por e-mail',
            'enviar'       => 'Enviar mensagem',
            'erro'         => 'Preencha todos os campos corretamente',
            'sucesso'      => 'Mensagem enviada com sucesso!'
        ],

        'footer' => [
            'copyright'        => 'Todos os direitos reservados.',
            'criacao-de-sites' => 'Criação de sites'
        ],

        '404' => 'Página não encontrada'
    ];
