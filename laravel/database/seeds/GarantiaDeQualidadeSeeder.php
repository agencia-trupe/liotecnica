<?php

use Illuminate\Database\Seeder;

class GarantiaDeQualidadeSeeder extends Seeder
{
    public function run()
    {
        DB::table('garantia_de_qualidade')->insert([
            'imagem' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
        ]);
    }
}
