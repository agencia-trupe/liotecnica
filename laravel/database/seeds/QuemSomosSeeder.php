<?php

use Illuminate\Database\Seeder;

class QuemSomosSeeder extends Seeder
{
    public function run()
    {
        DB::table('quem_somos')->insert([
            'imagem' => '',
            'texto_pt' => '',
            'subtitulo_1_pt' => '',
            'texto_1_pt' => '',
            'subtitulo_2_pt' => '',
            'texto_2_pt' => '',
            'texto_en' => '',
            'subtitulo_1_en' => '',
            'texto_1_en' => '',
            'subtitulo_2_en' => '',
            'texto_2_en' => '',
            'texto_es' => '',
            'subtitulo_1_es' => '',
            'texto_1_es' => '',
            'subtitulo_2_es' => '',
            'texto_2_es' => '',
        ]);
    }
}
