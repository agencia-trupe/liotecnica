<?php

use Illuminate\Database\Seeder;

class TrabalheConoscoSeeder extends Seeder
{
    public function run()
    {
        DB::table('trabalhe_conosco')->insert([
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
            'imagem' => '',
        ]);
    }
}
