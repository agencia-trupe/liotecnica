<?php

use Illuminate\Database\Seeder;

class MissaoEVisaoSeeder extends Seeder
{
    public function run()
    {
        DB::table('missao_e_visao')->insert([
            'imagem' => '',
            'subtitulo_1_pt' => '',
            'texto_1_pt' => '',
            'subtitulo_2_pt' => '',
            'texto_2_pt' => '',
            'subtitulo_3_pt' => '',
            'texto_3_pt' => '',
            'subtitulo_1_en' => '',
            'texto_1_en' => '',
            'subtitulo_2_en' => '',
            'texto_2_en' => '',
            'subtitulo_3_en' => '',
            'texto_3_en' => '',
            'subtitulo_1_es' => '',
            'texto_1_es' => '',
            'subtitulo_2_es' => '',
            'texto_2_es' => '',
            'subtitulo_3_es' => '',
            'texto_3_es' => '',
        ]);
    }
}
