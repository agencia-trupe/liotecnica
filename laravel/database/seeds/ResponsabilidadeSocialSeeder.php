<?php

use Illuminate\Database\Seeder;

class ResponsabilidadeSocialSeeder extends Seeder
{
    public function run()
    {
        DB::table('responsabilidade_social')->insert([
            'imagem' => '',
            'texto_pt' => '',
            'texto_en' => '',
            'texto_es' => '',
        ]);
    }
}
