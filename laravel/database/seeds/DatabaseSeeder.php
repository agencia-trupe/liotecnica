<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(UserTableSeeder::class);
		$this->call(TrabalheConoscoSeeder::class);
		$this->call(ResponsabilidadeSocialSeeder::class);
		$this->call(QuemSomosSeeder::class);
		$this->call(MissaoEVisaoSeeder::class);
		$this->call(GarantiaDeQualidadeSeeder::class);
        $this->call(ContatoTableSeeder::class);

        Model::reguard();
    }
}
