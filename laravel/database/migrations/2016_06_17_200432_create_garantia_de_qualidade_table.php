<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarantiaDeQualidadeTable extends Migration
{
    public function up()
    {
        Schema::create('garantia_de_qualidade', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('garantia_de_qualidade');
    }
}
