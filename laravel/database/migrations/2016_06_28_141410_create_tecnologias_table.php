<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTecnologiasTable extends Migration
{
    public function up()
    {
        Schema::create('tecnologias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('imagem');
            $table->string('infografico_pt');
            $table->string('infografico_en');
            $table->string('infografico_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('tecnologias');
    }
}
