<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('telefone');
            $table->text('contato_pt');
            $table->text('contato_en');
            $table->text('contato_es');
            $table->text('localizacao_pt');
            $table->text('localizacao_en');
            $table->text('localizacao_es');
            $table->text('google_maps');
            $table->string('facebook');
            $table->string('linkedin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
