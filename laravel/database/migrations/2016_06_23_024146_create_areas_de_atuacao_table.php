<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasDeAtuacaoTable extends Migration
{
    public function up()
    {
        Schema::create('areas_de_atuacao', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->string('slug');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('capa');
            $table->string('imagem');
            $table->timestamps();
        });

        Schema::create('areas_de_atuacao_subcategorias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('areas_de_atuacao_id')->unsigned()->nullable();
            $table->integer('ordem')->default(0);
            $table->string('cor');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->string('titulo_es');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->timestamps();
            $table->foreign('areas_de_atuacao_id')->references('id')->on('areas_de_atuacao')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::drop('areas_de_atuacao_subcategorias');
        Schema::drop('areas_de_atuacao');
    }
}
