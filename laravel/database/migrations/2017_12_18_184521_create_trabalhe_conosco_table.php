<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrabalheConoscoTable extends Migration
{
    public function up()
    {
        Schema::create('trabalhe_conosco', function (Blueprint $table) {
            $table->increments('id');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->text('texto_es');
            $table->string('link');
            $table->string('botao_pt');
            $table->string('botao_en');
            $table->string('botao_es');
            $table->string('imagem');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('trabalhe_conosco');
    }
}
