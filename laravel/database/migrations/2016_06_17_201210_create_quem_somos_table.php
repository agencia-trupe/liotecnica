<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuemSomosTable extends Migration
{
    public function up()
    {
        Schema::create('quem_somos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->text('texto_pt');
            $table->string('subtitulo_1_pt');
            $table->text('texto_1_pt');
            $table->string('subtitulo_2_pt');
            $table->text('texto_2_pt');
            $table->text('texto_en');
            $table->string('subtitulo_1_en');
            $table->text('texto_1_en');
            $table->string('subtitulo_2_en');
            $table->text('texto_2_en');
            $table->text('texto_es');
            $table->string('subtitulo_1_es');
            $table->text('texto_1_es');
            $table->string('subtitulo_2_es');
            $table->text('texto_2_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('quem_somos');
    }
}
