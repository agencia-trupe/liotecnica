<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissaoEVisaoTable extends Migration
{
    public function up()
    {
        Schema::create('missao_e_visao', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem');
            $table->string('subtitulo_1_pt');
            $table->text('texto_1_pt');
            $table->string('subtitulo_2_pt');
            $table->text('texto_2_pt');
            $table->string('subtitulo_3_pt');
            $table->text('texto_3_pt');
            $table->string('subtitulo_1_en');
            $table->text('texto_1_en');
            $table->string('subtitulo_2_en');
            $table->text('texto_2_en');
            $table->string('subtitulo_3_en');
            $table->text('texto_3_en');
            $table->string('subtitulo_1_es');
            $table->text('texto_1_es');
            $table->string('subtitulo_2_es');
            $table->text('texto_2_es');
            $table->string('subtitulo_3_es');
            $table->text('texto_3_es');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('missao_e_visao');
    }
}
