-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: liotecnica
-- ------------------------------------------------------
-- Server version	5.7.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `areas_de_atuacao`
--

DROP TABLE IF EXISTS `areas_de_atuacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_de_atuacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_de_atuacao`
--

LOCK TABLES `areas_de_atuacao` WRITE;
/*!40000 ALTER TABLE `areas_de_atuacao` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas_de_atuacao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas_de_atuacao_subcategorias`
--

DROP TABLE IF EXISTS `areas_de_atuacao_subcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `areas_de_atuacao_subcategorias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `areas_de_atuacao_id` int(10) unsigned DEFAULT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `areas_de_atuacao_subcategorias_areas_de_atuacao_id_foreign` (`areas_de_atuacao_id`),
  CONSTRAINT `areas_de_atuacao_subcategorias_areas_de_atuacao_id_foreign` FOREIGN KEY (`areas_de_atuacao_id`) REFERENCES `areas_de_atuacao` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas_de_atuacao_subcategorias`
--

LOCK TABLES `areas_de_atuacao_subcategorias` WRITE;
/*!40000 ALTER TABLE `areas_de_atuacao_subcategorias` DISABLE KEYS */;
/*!40000 ALTER TABLE `areas_de_atuacao_subcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `cor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contato_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `contato_en` text COLLATE utf8_unicode_ci NOT NULL,
  `contato_es` text COLLATE utf8_unicode_ci NOT NULL,
  `localizacao_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `localizacao_en` text COLLATE utf8_unicode_ci NOT NULL,
  `localizacao_es` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'@','','','','','','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assunto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `garantia_de_qualidade`
--

DROP TABLE IF EXISTS `garantia_de_qualidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `garantia_de_qualidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `garantia_de_qualidade`
--

LOCK TABLES `garantia_de_qualidade` WRITE;
/*!40000 ALTER TABLE `garantia_de_qualidade` DISABLE KEYS */;
INSERT INTO `garantia_de_qualidade` VALUES (1,'','','','',NULL,NULL);
/*!40000 ALTER TABLE `garantia_de_qualidade` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_06_17_152339_create_banners_table',1),('2016_06_17_200432_create_garantia_de_qualidade_table',1),('2016_06_17_200914_create_missao_e_visao_table',1),('2016_06_17_201210_create_quem_somos_table',1),('2016_06_17_203638_create_responsabilidade_social_table',1),('2016_06_20_174725_create_newsletter_table',1),('2016_06_23_024146_create_areas_de_atuacao_table',1),('2016_06_28_141410_create_tecnologias_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `missao_e_visao`
--

DROP TABLE IF EXISTS `missao_e_visao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `missao_e_visao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_3_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_3_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_en` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_es` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_es` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_3_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_3_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `missao_e_visao`
--

LOCK TABLES `missao_e_visao` WRITE;
/*!40000 ALTER TABLE `missao_e_visao` DISABLE KEYS */;
INSERT INTO `missao_e_visao` VALUES (1,'','','','','','','','','','','','','','','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `missao_e_visao` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quem_somos`
--

DROP TABLE IF EXISTS `quem_somos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quem_somos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_1_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_es` text COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo_2_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quem_somos`
--

LOCK TABLES `quem_somos` WRITE;
/*!40000 ALTER TABLE `quem_somos` DISABLE KEYS */;
INSERT INTO `quem_somos` VALUES (1,'','','','','','','','','','','','','','','','',NULL,NULL);
/*!40000 ALTER TABLE `quem_somos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `responsabilidade_social`
--

DROP TABLE IF EXISTS `responsabilidade_social`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `responsabilidade_social` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `responsabilidade_social`
--

LOCK TABLES `responsabilidade_social` WRITE;
/*!40000 ALTER TABLE `responsabilidade_social` DISABLE KEYS */;
INSERT INTO `responsabilidade_social` VALUES (1,'','','','',NULL,NULL);
/*!40000 ALTER TABLE `responsabilidade_social` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tecnologias`
--

DROP TABLE IF EXISTS `tecnologias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tecnologias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_es` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_es` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `infografico` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tecnologias`
--

LOCK TABLES `tecnologias` WRITE;
/*!40000 ALTER TABLE `tecnologias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tecnologias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$f7eFIzy/FtZn4O8TK40asuqA7dh0lJmwZD.b6nV0GquvojVyW0ugK',NULL,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-28 14:35:48
